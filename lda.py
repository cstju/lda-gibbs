# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-12-27 14:59:52
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-12-27 19:58:46
import re
import numpy as np
from utils import choose

"""
Author: 
davidmcclure (https://github.com/davidmcclure)
Alex Kong (https://github.com/hitalex)

Reference:
Gibbs sampling update
@article{heinrich2005parameter,
  title={Parameter estimation for text analysis},
  author={Heinrich, G.},
  journal={Web: http://www.arbylon.net/publications/text-est.pdf},
  year={2005}
}
"""

np.set_printoptions(threshold='nan')

class Document(object):

    '''
    Splits a text file into an ordered list of words.
    '''

    # List of punctuation characters to scrub. Omits, the single apostrophe,
    # which is handled separately so as to retain contractions.
    PUNCTUATION = ['(', ')', ':', ';', ',', '-', '!', '.', '?', '/', '"', '*']

    # Carriage return strings, on *nix and windows.
    CARRIAGE_RETURNS = ['\n', '\r\n']

    # Final sanity-check regex to run on words before they get
    # pushed onto the core words list.
    WORD_REGEX = "^[a-z']+$"


    def __init__(self, filepath):
        '''
        Set source file location, build contractions list, and initialize empty
        lists for lines and words.
        '''
        self.filepath = filepath
        self.file = open(self.filepath)
        self.lines = []
        self.words = []


    def split(self, STOP_WORDS_SET):
        '''
        Split file into an ordered list of words. Scrub out punctuation;
        lowercase everything; preserve contractions; disallow strings that
        include non-letters.
        '''
        self.lines = [line for line in self.file]
        for line in self.lines:
            words = line.split(' ')
            for word in words:
                clean_word = self._clean_word(word)
                if clean_word and (clean_word not in STOP_WORDS_SET) and (len(clean_word) > 1): # omit stop words
                    self.words.append(clean_word)


    def _clean_word(self, word):
        '''
        Parses a space-delimited string from the text and determines whether or
        not it is a valid word. Scrubs punctuation, retains contraction
        apostrophes. If cleaned word passes final regex, returns the word;
        otherwise, returns None.
        '''
        word = word.lower()#将单词全部变为小写
        for punc in Document.PUNCTUATION + Document.CARRIAGE_RETURNS:
            word = word.replace(punc, '').strip("'")#将单词中的符号和回车全都删掉,并按照上引号将词分开
        return word if re.match(Document.WORD_REGEX, word) else None


class Corpus(object):

    '''
    A collection of documents.
    '''

    def __init__(self):
        '''
        Initialize empty document list.
        '''
        self.documents = []


    def add_document(self, document):
        '''
        Add a document to the corpus.
        '''
        self.documents.append(document)


    def build_vocabulary(self):
        '''
        Construct a list of unique words in the corpus.
        '''
        # ** ADD ** #
        # exclude words that appear in 90%+ of the documents
        # exclude words that are too (in)frequent
        discrete_set = set()
        for document in self.documents:
            for word in document.words:
                discrete_set.add(word)
        self.vocabulary = list(discrete_set)
        


    def lda(self, number_of_topics, iterations, alpha, beta):

        '''
        Model topics.
        '''
        print "Gibbs sampling process..."
        # Get vocabulary and number of documents.
        self.build_vocabulary()
        number_of_documents = len(self.documents)
        vocabulary_size = len(self.vocabulary)
        self.vocabularys_beta = vocabulary_size*beta
        self.topics_alpha = number_of_topics*alpha

        # Create the counter arrays.

        # 构建文章-主题统计个数矩阵
        self.document_topic_counts = np.zeros([number_of_documents, number_of_topics], dtype=np.int)
        # 构建主题-单词统计个数矩阵
        self.topic_word_counts = np.zeros([number_of_topics, vocabulary_size], dtype=np.int)
        # 构建主题统计个数向量
        self.topic_counts = np.zeros(number_of_topics)
        # 构建每篇文章统计单词个数向量
        self.document_counts = np.zeros(number_of_documents)

        self.current_word_topic_assignments = []

        

        #计算每个文章下的各个主题概率
        self.theta = np.zeros([number_of_documents, number_of_topics], dtype = np.float)
        #计算每个主题下的各个单词概率
        self.phi = np.zeros([number_of_topics, vocabulary_size], dtype = np.float)

        # Initialize
        print "Initializing..."
        for d_index, document in enumerate(self.documents):
            word_topic_assignments = []#每一篇文章都有一个其包含的单词对应的主题列表
            self.document_counts[d_index] = len(document.words)
            for word in document.words:
                if word in self.vocabulary:
                    # Select random starting topic assignment for word.
                    w_index = self.vocabulary.index(word)
                    starting_topic_index = np.random.randint(number_of_topics) # 初始随机给当前文章的当前单词指定一个主题
                    word_topic_assignments.append(starting_topic_index)# 将主题id添加到当前文章的单词中
                    # Set current topic assignment, increment doc-topic and word-topic counters.
                    self.document_topic_counts[d_index, starting_topic_index] += 1
                    self.topic_word_counts[starting_topic_index, w_index] += 1
                    self.topic_counts[starting_topic_index] += 1
            #第一维度是文章个数，第二维度是每个文章的单词个数，值是此单词对应的主题id
            self.current_word_topic_assignments.append(np.array(word_topic_assignments))

        # Run the sampler.
        for iteration in range(iterations):
            print "Iteration #" + str(iteration + 1) + "..."
            for d_index, document in enumerate(self.documents):
                for w, word in enumerate(document.words):
                    if word in self.vocabulary:
                        w_index = self.vocabulary.index(word)
                        # Get the topic that the word is currently assigned to.
                        current_topic_index = self.current_word_topic_assignments[d_index][w]#获得该文章中该单词当前主题id
                        # Decrement counts.
                        self.document_topic_counts[d_index, current_topic_index] -= 1#将当前主题id统计减1
                        self.topic_word_counts[current_topic_index, w_index] -= 1
                        self.topic_counts[current_topic_index] -= 1
                        self.document_counts[d_index] -= 1
                        # Get new topic.
                        topic_distribution = (self.topic_word_counts[:, w_index] + beta) \
                                             / (self.topic_counts + self.vocabularys_beta) \
                                             *(self.document_topic_counts[d_index] + alpha) \
                                             /(self.document_counts[d_index] + self.topics_alpha)
                             
                        #new_topic_index = np.random.multinomial(1, np.random.dirichlet(topic_distribution)).argmax()
                        # choose a new topic index according to topic distribution
                        new_topic_index = choose(range(number_of_topics), topic_distribution)#根据计算的主题概率分布选取新的主题
                        # Reassign and notch up counts.
                        self.current_word_topic_assignments[d_index][w] = new_topic_index
                        self.document_topic_counts[d_index, new_topic_index] += 1
                        self.topic_word_counts[new_topic_index, w_index] += 1
                        self.topic_counts[new_topic_index] += 1
                        self.document_counts[d_index] += 1

        for document_index in xrange(number_of_documents):
        	#self.theta的行数是文章数，列数是主题数，表示一个文章的各个主题概率
            self.theta[document_index] = (self.document_topic_counts[document_index] + alpha) \
                                        / (self.document_counts[document_index] + self.topics_alpha)

        for topic_index in xrange(number_of_topics):
        	#self.phi的行数是主题数，列数是单词数，表示一个主题的各个单词概率
            self.phi[topic_index] = (self.topic_word_counts[topic_index] + beta) \
                                        / (self.topic_counts[topic_index] + self.vocabularys_beta)
        
        
