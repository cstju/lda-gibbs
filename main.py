# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-12-20 17:22:47
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-12-27 19:49:28
import os
import glob
import sys
from operator import itemgetter # for sort
import lda

STOP_WORDS_SET = set()

def print_topic_word_distribution(corpus, number_of_topics, topk, filepath):
    """
    Print topic-word distribution to file and list @topk most probable words for each topic
    """
    print "Writing topic-word distribution to file: " + filepath
    V = len(corpus.vocabulary) # size of vocabulary
    assert(topk < V)
    f = open(filepath, "w")
    for k in range(number_of_topics):
        word_distribution = corpus.phi[k]
        word_index_pi = []
        for i in range(V):
            word_index_pi.append([i, word_distribution[i]])
        word_index_pi = sorted(word_index_pi, key=itemgetter(1), reverse=True) # sort by word count
        f.write("Topic #" + str(k) + ":\n")
        for i in range(topk):
            index = word_index_pi[i][0]
            f.write(corpus.vocabulary[index] + " ")
        f.write("\n")
    f.close()
    
def print_document_topic_distribution(corpus, number_of_topics, topk, filepath):
    """
    Print document-topic distribution to file and list @topk most probable topics for each document
    """
    print "Writing document-topic distribution to file: " + filepath
    assert(topk < number_of_topics)
    f = open(filepath, "w")
    D = len(corpus.documents) # number of documents
    for d in range(D):
        topic_distribution = corpus.theta[d]
        topic_index_pi = []
        for i in range(number_of_topics):
            topic_index_pi.append([i, topic_distribution[i]])
        topic_index_pi = sorted(topic_index_pi, key=itemgetter(1), reverse=True)
        f.write("Document #" + str(d) + ":\n")
        for i in range(topk):
            index = topic_index_pi[i][0]
            f.write("topic" + str(index) + " ")
        f.write("\n")
        
    f.close()
        
def main(argv):
    print "Usage: python ./main.py <number_of_topics> <alpha> <beta> <maxiteration>"
    # load stop words list from file
    # stopwords中存的是无实际意义的词
    stopwordsfile = open("stopwords.txt", "r")
    for word in stopwordsfile: # a stop word in each line
        word = word.replace("\n", '')
        word = word.replace("\r\n", '')
        STOP_WORDS_SET.add(word)
    
    corpus = lda.Corpus() # instantiate corpus
    # iterate over the files in the directory.
    document_paths = ['./texts/grimm_fairy_tales', './texts/tech_blog_posts', './texts/nyt']
    for document_path in document_paths:
        for document_file in glob.glob(os.path.join(document_path, '*.txt')):
            document = lda.Document(document_file) # instantiate document
            document.split(STOP_WORDS_SET) # tokenize
            corpus.add_document(document) # push onto corpus documents list

    corpus.build_vocabulary()
    print "Vocabulary size:" + str(len(corpus.vocabulary))
    print "Number of documents:" + str(len(corpus.documents))
    
    number_of_topics = int(argv[1])
    alpha = float(argv[2]) # alpha = 50 / iterations
    beta = float(argv[3]) # beta = 0.01
    max_iterations = int(argv[4])
    corpus.lda(number_of_topics, max_iterations, alpha, beta)

    doc_topK_topic = 10
    topic_topK_vocabulary = 20
    
    print_topic_word_distribution(corpus, number_of_topics, topic_topK_vocabulary, "./topic-word.txt")
    print_document_topic_distribution(corpus, number_of_topics, doc_topK_topic, "./document-topic.txt")
    
if __name__ == "__main__":
    main(sys.argv)
